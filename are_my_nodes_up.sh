#!/bin/bash -e

#rm *.html
#wget http://discovery.skycoin.net:8001
grep -Fo --file=all_node_pubkeys.txt index.html > greped.tmp && echo "Step 0/3"

sort all_node_pubkeys.txt -o pub_sorted.tmp && echo "Step 1/3"
sort greped.tmp -o greped_sorted.tmp && echo "Step 2/3"
#STATUS=$(diff pub_sorted.tmp greped_sorted.tmp  --suppress-common-lines -y)
diff pub_sorted.tmp greped_sorted.tmp  --suppress-common-lines -y || echo "!! We have a problem !! "

#echo " : $STATUS"
echo "Step 3/3"
