#!/bin/bash -e
password="change me !"
echo -e "$password\n$password" | passwd root
echo -e "$password\n$password" | passwd pi
apt update && sudo apt dist-upgrade -y
apt purge xorg mate-desktop-environment -y
apt install wget git rsync -y

## SKY HERE ##


cd ~
#arm 64bit
wget https://dl.google.com/go/go1.10.2.linux-arm64.tar.gz
#https://dl.google.com/go/go1.9.6.linux-armv6l.tar.gz 
tar xvf go*.tar.gz
cd go

echo "export GOPATH=$HOME/go" >> ~/.bash_profile
echo "export GOBIN=$HOME/go/bin" >> ~/.bash_profile
echo "export PATH=$PATH:/root/go/bin" >> ~/.bash_profile
source ~/.bash_profile

mkdir -p $GOPATH/src/github.com/skycoin
cd $GOPATH/src/github.com/skycoin
git clone https://github.com/skycoin/skywire.git

cd $GOPATH/src/github.com/skycoin/skywire/cmd
go install ./...

# Manager
#cd $GOPATH/bin
#nohup ./manager -web-dir ${GOPATH}/src/github.com/skycoin/skywire/static/skywire-manager > /dev/null 2>&1 & echo $! > manager.pid

# Node
cd $GOPATH/bin
nohup ./node -connect-manager -manager-address 192.168.0.33:5998 -manager-web 192.168.0.33:8000  -discovery-address discovery.skycoin.net:5999-034b1cd4ebad163e457fb805b3ba43779958bba49f2c5e1e8b062482904bacdb68  > /dev/null 2>&1 & echo $! > node.pid

echo "Installed, but not launched!"
echo -e "DONE DONE \n DONE DONE \n DONE DONE"
