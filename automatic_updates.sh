#!/bin/bash

sudo apt update && sudo apt dist-upgrade -y
sudo apt install unattended-upgrades apt-listchanges -y

cd config
sudo cp 50unattended-upgrades 20auto-upgrades /etc/apt/apt.conf.d/ -f
sudo systemctl restart unattended-upgrades
sudo systemctl enable unattended-upgrades
sudo systemctl status unattended-upgrades
cd -
