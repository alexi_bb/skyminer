#!/bin/bash -e 

#touch ssh in boot part e.g. /dev/sdb1

sudo apt install && sudo apt dist-upgrade -y
sudo apt install git wget rsync

cd ~
#arm 32bit
wget https://dl.google.com/go/go1.9.6.linux-armv6l.tar.gz 
tar xvf go*.tar.gz
cd go

echo "export GOPATH=$HOME/go" >> ~/.bash_profile
echo "export GOBIN=$HOME/go/bin" >> ~/.bash_profile
echo "export PATH=$PATH:/home/pi/go/bin" >> ~/.bash_profile
source ~/.bash_profile

mkdir -p $GOPATH/src/github.com/skycoin
cd $GOPATH/src/github.com/skycoin
git clone https://github.com/skycoin/skywire.git

cd $GOPATH/src/github.com/skycoin/skywire/cmd
go install ./...

# Manager
#cd $GOPATH/bin
#nohup ./node -connect-manager -manager-address :5998 -manager-web :8000 -discovery-address discovery.skycoin.net:5999-034b1cd4ebad163e457fb805b3ba43779958bba49f2c5e1e8b062482904bacdb68 -address :5000 -web-port :6001 > /dev/null 2>&1 &cd /

# Node
cd $GOPATH/bin
#nohup ./node -connect-manager -manager-address your-manager-IP:5998 -manager-web your-manager-IP:8000 -discovery-address discovery.skycoin.net:5999-034b1cd4ebad163e457fb805b3ba43779958bba49f2c5e1e8b062482904bacdb68 -address :5000 -web-port :6001 > /dev/null 2>&1 &cd /
nohup ./node -connect-manager -manager-address 192.168.0.2:5998 -manager-web 192.168.0.2:8000  -discovery-address discovery.skycoin.net:5999-034b1cd4ebad163e457fb805b3ba43779958bba49f2c5e1e8b062482904bacdb68  > /dev/null 2>&1 & echo $! > node.pid
