#!/bin/bash -e

passwd <<EOF
oldpw
newpw
newpw
EOF

sudo apt update && sudo apt dist-upgrade -y
sudo apt purge xorg mate-desktop-environment -y #if x happends to be installed
sudo apt install wget git rsync -y

## SKY HERE ##


cd ~
#arm 64bit
#wget https://dl.google.com/go/go1.10.2.linux-arm64.tar.gz

# !! might want to update this
wget https://dl.google.com/go/go1.9.6.linux-armv6l.tar.gz 
tar xvf go*.tar.gz
cd go

echo "export GOPATH=$HOME/go" >> ~/.bash_profile
echo "export GOBIN=$HOME/go/bin" >> ~/.bash_profile
echo "export PATH=$PATH:$HOME/go/bin" >> ~/.bash_profile
source ~/.bash_profile

mkdir -p $GOPATH/src/github.com/skycoin
cd $GOPATH/src/github.com/skycoin
git clone https://github.com/skycoin/skywire.git

cd $GOPATH/src/github.com/skycoin/skywire/cmd
go install ./...

cd ~/pi_install
sudo cp lib / -Rv

sudo systemctl stop manager
sudo systemctl disable manager

sudo systemctl start node
sudo systemctl enable node
#sudo systemctl status node
cd ~
sudo reboot
# Manager
#cd $GOPATH/bin
#nohup ./manager -web-dir ${GOPATH}/src/github.com/skycoin/skywire/static/skywire-manager > /dev/null 2>&1 & echo $! > manager.pid

# Node
#cd $GOPATH/bin
#nohup ./node -connect-manager -manager-address 192.168.0.33:5998 -manager-web 192.168.0.33:8000  -discovery-address discovery.skycoin.net:5999-034b1cd4ebad163e457fb805b3ba43779958bba49f2c5e1e8b062482904bacdb68  > /dev/null 2>&1 & echo $! > node.pid

echo -e "\n\nDONE DONE \n DONE DONE \n DONE DONE"
